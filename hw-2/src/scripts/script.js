const burger = document.querySelector('.burger');
const menu = document.querySelector('header nav')
const image = burger.querySelector('img');

burger.addEventListener('click', () => {
    burger.classList.toggle('open');
    menu.classList.toggle('open');

    if (burger.classList.contains('open')) {
        image.src = './images/close.svg'
    } else {
        image.src = './images/burger.svg'
    }
})




